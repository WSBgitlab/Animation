const params = {
    particles: {
        number: {
          value: 80,
          density: {
            enable: true,
            value_area: 800
          }
        },
        color : {
            value : "#fff"
        },
        shape : {
            type :  "circle",
            
            stroke : {
                width : 0,
                color :  "#010101", 
            },
            polygon : {
                nb_sides : 5
            },
            image : {
                src :  "img/github.svg" ,
                width : 100,
                height : 100
            }
      },
      size : {
        value : 3,
        random : true,
        anim : {
            enable : false,
            speed : 40,
            size_min : 0.1,
            sync : false
        }
      },
      opacity : {	
        value : 100,
        random : true,
            anim : {
                enable : false,
                speed : 80,
                opacity_min : 0.1,
                sync : true
            }
        },
        move : {
            random : true,
            speed : 6.5,
            out_mode : "bounce"
        }
    }
}
