//Selecionando todos os documentos que contém [data-anime]
const target = document.querySelectorAll('[data-anime]');
//Nome da classe
const animate = 'animate';
/**
 * 1 - Verificar a distância da barra de rolagem!
 * 2 - Verificar a distância do 1) + offset é maior do que a distância entre o elemento e o Topo da página!.
 * 3 - Adicionar a classe caso verdadeiro e remover caso falso! 
 * 4 - Ativar a função de animação toda vez o usuário utilizar o Scroll.
 * 5 - Otimizar ativação.
 
*/

//Função irá acionar toda vez que o scroll for acionado pelo usuário.

/**
 *  Calculo ultilizado pelo tutorial para aproximar o efeito quando a div(document),
 *  const windowTop = window.pageYOffset + (window.innerHeight * 3) / 4;
 *  estiver no footer na tela
 *   -------------------------------
 *  |                               |
 *  |                               |
 *  |    Tela Principal             |
 *  |                               |
 *  |-------------------------------|
 *  |                               |
 *  |   Div com efeito              |
 *  |                               |
 *   -------------------------------
 */

//funcionalidade debounce da lib Lodash
/**
 * Função debounce para não ficar acionando a função desnecessáriamente 
 */
const debounce = (func, wait,immediate) => {
    let timeout;
    return function(...args){
        const context = this;
        const later = () => {
            timeout = null;
            if(!immediate) func.apply(context,args);

        };
        const callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function animateScroll(){
    const windowTop = window.pageYOffset + (window.innerHeight * 3) / 4; 
    target.forEach(element =>{
        if((windowTop) > element.offsetTop){
            element.classList.add('animate');
        }else{
            element.classList.remove('animate');
        }
    });
}

//ativando a função ao load da página!
animateScroll();

if(target.length){
    window.addEventListener('scroll', debounce(() => {
        //chamando função scroll
        console.log();
        animateScroll();
        console.log('asdasd');
    },100));
}